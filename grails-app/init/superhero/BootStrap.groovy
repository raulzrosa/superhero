package superhero

import org.grails.web.json.JSONObject

import java.lang.reflect.Array

class BootStrap {

    def init = { servletContext ->
        new User(username: "user", password: "81dc9bdb52d04dc20036dbd8313ed055").save()

        new Hero(name: "Iron Man", description: "Wounded, captured and forced to build a weapon by his enemies, " +
                "billionaire industrialist Tony Stark instead created an advanced suit of armor to save his life " +
                "and escape captivity. Now with a new outlook on life, Tony uses his money and intelligence to make " +
                "the world a safer, better place as Iron Man.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55.jpg", dead: false).save()

        new Hero(name: "Thor", description: "As the Norse God of thunder and lightning, Thor wields one of the " +
                "greatest weapons ever made, the enchanted hammer Mjolnir. While others have described Thor as an " +
                "over-muscled, oafish imbecile, he's quite smart and compassionate.  He's self-assured, and he would " +
                "never, ever stop fighting for a worthwhile cause.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/d/d0/5269657a74350.jpg", dead: false).save()

        new Hero(name: "Spider-Man", description: "Bitten by a radioactive spider, high school student Peter Parker " +
                "gained the speed, strength and powers of a spider. Adopting the name Spider-Man, Peter hoped to " +
                "start a career using his new abilities. Taught that with great power comes great responsibility, " +
                "Spidey has vowed to use his powers to help people.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/3/50/526548a343e4b.jpg", dead: false).save()

        new Hero(name: "Hulk", description: "Caught in a gamma bomb explosion while trying to save the life of a " +
                "teenager, Dr. Bruce Banner was transformed into the incredibly powerful creature called the Hulk. " +
                "An all too often misunderstood hero, the angrier the Hulk gets, the stronger the Hulk gets.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0.jpg", dead: false).save()

        new Hero(name: "Captain America", description: "Vowing to serve his country any way he could, young Steve " +
                "Rogers took the super soldier serum to become America's one-man army. Fighting for the red, white " +
                "and blue for over 60 years, Captain America is the living, breathing symbol of freedom and liberty.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/3/50/537ba56d31087.jpg", dead: false).save()

        new Hero(name: "Scarlet Witch (Wanda Maximoff)", description: "The Scarlet Witch can tap into mystic " +
                "energy for reality-altering effects; this power was formerly limited to the creation of " +
                "'hex-spheres' of reality-disrupting quasi-psionic force to cause molecular disturbances in a " +
                "target's probability field, resulting in spontaneous combustion, deflection of objects in flight, " +
                "and so on. She later became able to alter reality on a far greater scale, creating entire armies " +
                "of enemies from nowhere. Although the Scarlet Witch has been trained in basic sorcery techniques, " +
                "she lacks the specialized mystic training required to fully control her power.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/6/70/5261a7d7c394b.jpg", dead: false).save()

        new Hero(name: "Captain Marvel (Carol Danvers)", description: "Ms. Marvel's current powers include flight, " +
                "enhanced strength, durability and the ability to shoot concussive energy bursts from her hands. " +
                "Ms. Marvel is a skilled pilot & hand to hand combatant",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/6/80/5269608c1be7a.jpg", dead: false).save()

        new Hero(name: "Jean Grey", description: "Jean Grey possessed telepathic powers enabling her to read minds, " +
                "project her thoughts into the minds of others, initiate astral travel, and mentally stun opponents" +
                " with pure psionic force, among other talents. She also possessed telekinesis, allowing her to " +
                "levitate and manipulate objects and others, generate force fields, fly, and stimulate heat molecules " +
                "to generate concussive blasts.Her powers were magnified to near-infinite levels while she served as " +
                "an avatar for the cosmic Phoenix Force. She was able to manipulate matter and energy on a molecular " +
                "scale, although this varied on the Force's status and how much power it chose to allocate to her.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/f/d0/528d3412090b4.jpg", dead: false).save()

        new Hero(name: "Black Panther", description: "T'Challa is a brilliant tactician, strategist, scientist, " +
                "tracker and a master of all forms of unarmed combat whose unique hybrid fighting style incorporates " +
                "acrobatics and aspects of animal mimicry. T'Challa being a royal descendent of a warrior race is " +
                "also a master of armed combat, able to use a variety of weapons but prefers unarmed combat. He is " +
                "a master planner who always thinks several steps ahead and will go to extreme measures to achieve " +
                "his goals and protect the kingdom of Wakanda.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/6/60/5261a80a67e7d.jpg", dead: false).save()

        new Hero(name: "Deadpool", description: "Deadpool possesses a superhuman healing factor derived from that of " +
                "the mutant Wolverine that allows him to regenerate damaged or destroyed areas of his cellular " +
                "structure at a rate far greater than that of an ordinary human. As such, he can regrow severed limbs " +
                "or vital organs.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/9/90/5261a86cacb99.jpg", dead: false).save()

        new Hero(name: "Kitty Pryde", description: "Kitty Pryde is a mutant with the ability to “phase” through solid " +
                "matter by shifting her atoms through the spaces between the atoms of the object through which she is " +
                "moving. Kitty can phase her clothing along with herself, and through practice has learned to phase " +
                "other objects and people without harm to them. For all intents and purposes, Kitty is intangible " +
                "whilst phasing, however she is still vulnerable to psionic or mystical attacks.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/4/03/5261677b30b64.jpg", dead: false).save()

        new Hero(name: "Black Widow", description: "Classified",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/f/30/50fecad1f395b.jpg", dead: false).save()

        new Hero(name: "Storm", description: "Ororo Monroe is the descendant of an ancient line of African priestesses, " +
                "all of whom have white hair, blue eyes, and the potential to wield magic.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/6/40/526963dad214d.jpg", dead: false).save()

        new Hero(name: "Wolverine", description: "Born with super-human senses and the power to heal from almost any " +
                "wound, Wolverine was captured by a secret Canadian organization and given an unbreakable skeleton " +
                "and claws. Treated like an animal, it took years for him to control himself. Now, he's a premiere " +
                "member of both the X-Men and the Avengers.",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/2/60/537bcaef0f6cf.jpg", dead: false).save()

        new Creator(firstName: "Stan", middleName: "", lastName: "Lee", suffix: "", fullName: "Stan Lee",
                thumbnail: "https://observatoriodocinema.bol.uol.com.br/wp-content/uploads/2017/02/Stan-Lee.jpg").save()

        new Creator(firstName: "John", middleName: "", lastName: "Romita", suffix: "", fullName: "John Romita",
                thumbnail: "http://pm1.narvii.com/6566/118d526aa1cf64cf1dae8781d79f7b9d909f1e44_00.jpg").save()

        new Creator(firstName: "John", middleName: "", lastName: "Romita", suffix: "Jr", fullName: "John Romita Jr.",
                thumbnail: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/John_Romita_Jr%2C_2006.jpg/250px-John_Romita_Jr%2C_2006.jpg").save()

        new Creator(firstName: "Jack", middleName: "", lastName: "Kirby", suffix: "", fullName: "Jack Kirby",
                thumbnail: "https://www.alexrossart.com/assets/images/Kirby100.jpg").save()

        new Creator(firstName: "Joe", middleName: "", lastName: "Quesada", suffix: "", fullName: "Joe Quesada",
                thumbnail: "http://www.farofeiros.com.br/wp-content/uploads/2017/10/Detalhes-do-roubo-da-casa-de-Joe-Quesada.jpg").save()

        new Creator(firstName: "Brian", middleName: "Michael", lastName: "Bendis", suffix: "", fullName: "Brian Michael Bendis",
                thumbnail: "https://jovemnerd.com.br/wp-content/uploads/2018/02/brian-michael-bendis-dc-760x428.png").save()

        new Comic(title: "Civil War", description: "The landscape of the Marvel Universe is changing, and it's " +
                "time to choose: Whose side are you on? A conflict has been brewing from more than a year, " +
                "threatening to pit friend against friend, brother against brother - and all it will take is a " +
                "single misstep to cost thousands their lives and ignite the fuse! As the war claims its first " +
                "victims, no one is safe as teams, friendships and families begin to fall apart. The crossover " +
                "that rewrites the rules, Civil War stars Spider-Man, the New Avengers, the Fantastic Four, the " +
                "X-Men and the entirety of the Marvel pantheon!", isbn: "978-0-7851-2178-7",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/8/c0/51dda501724ed.jpg").save()

        new Comic (title: "Planet Hulk (2015) #5", description: "BRANDED BY BATTLE! ABSOLVED BY BLOOD!" +
                " For STEVE ROGERS and DEVIL DINOSAUR...there is no end to HULK, no promise of salvation. Crawl in" +
                " the mud and the blood, until the claws of death reach your throat! THERE IS NO ESCAPE...PLANET " +
                "HULK IS INSIDE US ALL!", isbn: "",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/e/f0/55e5cbd701beb.jpg").save()

        new Comic (title: "The Amazing Spider-Man (1963) #121", description: "The death of Gwen Stacy. " +
                "'Nuff said.", isbn: "",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/a/00/57b71f662fdc8.jpg").save()

        new Comic (title: "The Amazing Spider-Man (1963) #122", description: "John Romita masterfully conveys Spidey's" +
                " sadness and rage in this cover. And that text? No lie. Gobby's going down.", isbn: "",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/9/60/57b72b0d9b47b.jpg").save()

        new Comic (title: "Thor: God of Thunder (2012) #1", description: "", isbn: "",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/b/f0/50f41aeea26d6.jpg").save()

        new Comic (title: "House of M (2015) #1", description: "The House of Magnus controls all, having " +
                "crushed all resistance to their reign. Magneto rules with an iron-fist, while his children" +
                " and grandchildren live their lives as royals-with all the privileges and pit-falls that " +
                "come with that title. But is Magneto's control as all-encompassing as it seems? And what " +
                "happens to a conqueror when there is no one left to conquer?", isbn: "",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/9/00/55ce50c4ea155.jpg").save()

        new Comic (title: "Secret War (2004) #1", description: "The most popular and acclaimed writer " +
                "in comics reveals the darkest chapter in MARVEL UNIVERSE history in this quarterly " +
                "limited series MEGA-EVENT! When Nick Fury finds a disturbing connection between many " +
                "of Marvel's deadliest villains, he puts together a ragtag team of the MU's most " +
                "misunderstood heroes for a secret mission to do what the U.S. government could never " +
                "allow, eventually leading to a super-powered blowout between a 'Who's Who' of NYC heroes " +
                "and mutants! Featuring the American debut of the stunning, fully painted work of Italian " +
                "artist Gabrielle Dell'otto.", isbn: "",
                thumbnail: "http://i.annihil.us/u/prod/marvel/i/mg/6/80/583cae868a052.jpg").save()

    }
    def destroy = {
    }
}
