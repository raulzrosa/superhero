package superhero

@Singleton
class DataUtil {

    long getLong (value) throws IllegalArgumentServiceException {
        try {
            return value as Long

        } catch (NumberFormatException | NullPointerException e) {
            throw new IllegalArgumentServiceException('input.id.invalid')
        }
    }

    double getDouble (value) throws IllegalArgumentServiceException {
        try {
            return value as Double

        } catch (NumberFormatException | NullPointerException e) {
            throw new IllegalArgumentServiceException('input.id.invalid')
        }
    }

    int getInteger (value) throws IllegalArgumentServiceException {
        try {
            return value as Integer

        } catch (NumberFormatException | NullPointerException e) {
            throw new IllegalArgumentServiceException('input.id.invalid')
        }
    }

    boolean getBoolean (value) throws IllegalArgumentServiceException {
        try {
            if (value instanceof String) {
                return Boolean.parseBoolean(value)
            }

            return value as Boolean

        } catch (NumberFormatException | NullPointerException e) {
            throw new IllegalArgumentServiceException('input.id.invalid')
        }
    }
}