package superhero

import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject

@Singleton
class ComicUtil {

    JSONObject getComicJSONObject(Comic comic) {
        JSONObject comicJSONObject = new JSONObject(
                [
                        'id': comic.id,
                        'title': comic.title,
                        'description': comic.description,
                        'isbn': comic.isbn,
                        'thumbnail': comic.thumbnail
                ]
        )

        return comicJSONObject
    }

    JSONObject getComicJSONObjectArray(List<Comic> comics) {

        JSONObject responseJSONObject = new JSONObject()
        JSONArray comicsJSONArray = new JSONArray()

        comics.each { Comic comic ->
            JSONObject comicJSONObject = new JSONObject()
            comicJSONObject = getComicJSONObject(comic)

            comicsJSONArray.put(comicJSONObject)
        }

        return responseJSONObject.put('comics', comicsJSONArray)
    }
}
