package superhero

import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import grails.converters.JSON

@Singleton
class HeroUtil {

    JSONObject getHeroJSONObject(Hero hero) {
        JSONObject heroJSONObject = new JSONObject(
                [
                    'id': hero.id,
                    'name': hero.name,
                    'description': hero.description,
                    'thumbnail': hero.thumbnail,
                    'dead': hero.dead
                ]
        )

        return heroJSONObject
    }

    JSONObject getHeroJSONObjectArray(List<Hero> heroes) {

        JSONObject responseJSONObject = new JSONObject()
        JSONArray heroesJSONArray = new JSONArray()

        heroes.each { Hero hero ->
            JSONObject heroJSONObject = new JSONObject()
            heroJSONObject = getHeroJSONObject(hero)

            heroesJSONArray.put(heroJSONObject)
        }

        return responseJSONObject.put('heroes', heroesJSONArray)
    }
}
