package superhero

import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject

@Singleton
class CreatorUtil {

    JSONObject getCreatorJSONObject(Creator creator) {
        JSONObject creatorJSONObject = new JSONObject(
                [
                        'id': creator.id,
                        'firstName': creator.firstName,
                        'middleName': creator.middleName,
                        'lastName': creator.lastName,
                        'suffix': creator.suffix,
                        'fullName': creator.fullName,
                        'thumbnail': creator.thumbnail
                ]
        )

        return creatorJSONObject
    }

    JSONObject getCreatorJSONObjectArray(List<Creator> creators) {

        JSONObject responseJSONObject = new JSONObject()
        JSONArray creatorsJSONArray = new JSONArray()

        creators.each { Creator creator ->
            JSONObject creatorJSONObject = new JSONObject()
            creatorJSONObject = getCreatorJSONObject(creator)

            creatorsJSONArray.put(creatorJSONObject)
        }

        return responseJSONObject.put('creators', creatorsJSONArray)
    }
}

