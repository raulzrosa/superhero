package superhero

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import groovy.json.JsonSlurper
import org.grails.web.json.JSONObject
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Transactional
class HeroService {

    MessageSource messageSource

    def serviceMethod() {

    }

    def delete() {

    }

    def findAllOrderByName(HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        try {
            List<Hero> heroes = Hero.findAll().sort( { it.name.toLowerCase() } )
            responseJSONObject = HeroUtil.instance.getHeroJSONObjectArray(heroes)
            response.status = HttpStatus.OK.value()
        } catch(IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }
        return responseJSONObject
    }

    def show(GrailsParameterMap params, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()

        try {
            Hero hero = Hero.read(DataUtil.instance.getLong(params.id))

            if(!hero) {
                throw new IllegalArgumentServiceException(HttpStatus.NOT_FOUND.value())
            }
            responseJSONObject = HeroUtil.instance.getHeroJSONObject(hero)
            response.status = HttpStatus.OK.value()

        } catch(IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }
        return responseJSONObject
    }

    JSONObject save(HttpServletRequest request, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        JSONObject jsonObject = request.JSON

        try {
            JsonSlurper jsonSlurper = new JsonSlurper()
            Hero hero = new Hero(jsonSlurper.parseText(jsonObject.toString()))

            if(!hero.validate()) {
                throw new IllegalArgumentServiceException(hero.errors)
            }

            hero.save(flush: true, failOnError: true)

            responseJSONObject = HeroUtil.instance.getHeroJSONObject(hero)

            response.status = HttpStatus.CREATED.value()

        } catch(IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }
        return responseJSONObject
    }

    def update(GrailsParameterMap params, HttpServletRequest request, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        JSONObject jsonObject = request.JSON

        Hero hero = Hero.findById(DataUtil.instance.getLong(params.id))

        if(hero) {
            try {
                JsonSlurper jsonSlurper = new JsonSlurper()
                hero.properties = jsonSlurper.parseText(jsonObject.toString())
                if (!hero.validate()) {
                    throw new IllegalArgumentServiceException(hero.errors)
                }

                hero.save(flush: true, failOnError: true)

                responseJSONObject = HeroUtil.instance.getHeroJSONObject(hero)
                response.status = HttpStatus.OK.value()

            } catch(IllegalArgumentServiceException e) {
                responseJSONObject.put('errors', e.getMessages(messageSource))
                response.setStatus(e.getHttpCode())
            }
        } else {
            responseJSONObject = NotFoundResponseUtil.instance.createNotFoundResponse(messageSource.getMessage(null, null))
            response.status = HttpStatus.NOT_FOUND.value()
        }
        return responseJSONObject
    }
}
