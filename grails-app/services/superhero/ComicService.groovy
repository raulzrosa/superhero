package superhero

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import groovy.json.JsonSlurper
import org.grails.web.json.JSONObject
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Transactional
class ComicService {

    MessageSource messageSource

    def serviceMethod() {

    }

    def findAllOrderByName(HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        try {
            List<Comic> comics = Comic.findAll().sort( { it.title.toLowerCase() } )
            responseJSONObject = ComicUtil.instance.getComicJSONObjectArray(comics)
            response.status = HttpStatus.OK.value()
        } catch(IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }
        return responseJSONObject
    }

    def show(GrailsParameterMap params, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()

        try {
            Comic comic = Comic.read(DataUtil.instance.getLong(params.id))

            if(!comic) {
                responseJSONObject.put('errors', 'Não achou')
                response.setStatus(404)
                return responseJSONObject
            }
            responseJSONObject = ComicUtil.instance.getComicJSONObject(comic)
            response.status = HttpStatus.OK.value()

        } catch(IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }
        return responseJSONObject
    }

    JSONObject save(HttpServletRequest request, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        JSONObject jsonObject = request.JSON

        try {
            JsonSlurper jsonSlurper = new JsonSlurper()
            Comic comic = new Comic(jsonSlurper.parseText(jsonObject.toString()))

            if(!comic.validate()) {
                throw new IllegalArgumentServiceException(comic.errors)
            }

            comic.save(flush: true, failOnError: true)

            responseJSONObject = ComicUtil.instance.getComicJSONObject(comic)

            response.status = HttpStatus.CREATED.value()

        } catch(IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }
        return responseJSONObject
    }

}
