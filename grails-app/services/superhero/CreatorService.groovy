package superhero

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import groovy.json.JsonSlurper
import org.grails.web.json.JSONObject
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Transactional
class CreatorService {

    MessageSource messageSource

    def serviceMethod() {

    }

    def findAllOrderByName(HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        try {
            List<Creator> creators = Creator.findAll().sort( { it.firstName.toLowerCase() } )
            responseJSONObject = CreatorUtil.instance.getCreatorJSONObjectArray(creators)
            response.status = HttpStatus.OK.value()
        } catch(Exception e) {
            response.setStatus(HttpStatus.NOT_FOUND.value())
        }
        return responseJSONObject
    }

    def show(GrailsParameterMap params, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()

        try {
            Creator creator = Creator.read(DataUtil.instance.getLong(params.id))

            if(!creator) {
                responseJSONObject.put('errors', 'Not Found')
                response.setStatus(404)
                return responseJSONObject
            }
            responseJSONObject = CreatorUtil.instance.getCreatorJSONObject(creator)
            response.status = HttpStatus.OK.value()

        } catch(IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }
        return responseJSONObject
    }

    JSONObject save(HttpServletRequest request, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        JSONObject jsonObject = request.JSON

        try {
            JsonSlurper jsonSlurper = new JsonSlurper()
            Creator creator = new Creator(jsonSlurper.parseText(jsonObject.toString()))

            if(!creator.validate()) {
                throw new Exception()
            }

            creator.save(flush: true, failOnError: true)

            responseJSONObject = CreatorUtil.instance.getCreatorJSONObject(creator)

            response.status = HttpStatus.CREATED.value()

        } catch(Exception e) {
            responseJSONObject.put('errors', 'Couldn\'t create CREATOR')
            response.setStatus(500)
        }
        return responseJSONObject
    }

}
