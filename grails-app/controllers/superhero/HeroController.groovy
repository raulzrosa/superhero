package superhero

class HeroController {

	static allowedMethods = [delete: "DELETE", index: "GET", save: "POST", show: "GET", update: "PUT"]

    HeroService heroService

    def index() {
        render heroService.findAllOrderByName(response)
    }

    def delete() {

    }

    def show() {
        render heroService.show(params, response)
    }

    def save() {
        render heroService.save(request, response)
    }

    def update() {
        render heroService.update(params, request, response)
    }
}
