package superhero

class CreatorController {

    static allowedMethods = [delete: "DELETE", index: "GET", save: "POST", show: "GET", update: "PUT"]

    CreatorService creatorService

    def index() {
        render creatorService.findAllOrderByName(response)
    }

    def delete() {

    }

    def show() {
        render creatorService.show(params, response)
    }

    def save() {
        render creatorService.save(request, response)
    }

    def update() {
    }
}
