package superhero


import grails.rest.*
import grails.converters.*

class ComicController {

    static allowedMethods = [delete: "DELETE", index: "GET", save: "POST", show: "GET", update: "PUT"]

    ComicService comicService

    def index() {
        render comicService.findAllOrderByName(response)
    }

    def delete() {

    }

    def show() {
        render comicService.show(params, response)
    }

    def save() {
        render comicService.save(request, response)
    }

    def update() {
    }
}
