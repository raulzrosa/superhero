package superhero

class Creator {

    String firstName
    String middleName
    String lastName
    String suffix
    String fullName
    String thumbnail

    static hasMany = [comics: Comic]

    static constraints = {
        firstName nullable: false
        middleName nullable: true
        lastName nullable: false
        suffix nullable: true
        fullName nullable: false
        thumbnail nullable: false
    }
}
