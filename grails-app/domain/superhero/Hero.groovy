package superhero

class Hero {

    String name
    String description
    String thumbnail
    boolean dead

    static hasMany = [comics: Comic]

    static constraints = {
        name nullable: false, blank: false, maxSize: 255, unique: true
        description nullable: false, blank: false, maxSize: 2000
        thumbnail nullable: false, blank: false
        dead nullable: false
    }
    static mapping = {
        // myStringProperty defaultValue: "'Cash'"
        // myIntProperty defaultValue: "0"
        dead defaultValue: false
    }
}
