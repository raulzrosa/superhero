package superhero

class Comic {

    String title
    Integer issueNumber
    String description
    String isbn
    String thumbnail

    static hasMany = [heros: Hero, creators: Creator]

    static constraints = {
        title nullable: false
        issueNumber nullable: true
        description nullable: true, maxSize: 2000
        isbn nullable: true
        thumbnail nullable: false
    }
}
